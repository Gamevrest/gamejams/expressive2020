﻿using System;
using System.Collections.Generic;
using GamevrestUtils;
using Objects;
using UnityEngine;

namespace Player
{
    [Serializable]
    public struct OffsetCamera
    {
        public Transform camera;
        public Transform parent;
    }

    public class CameraManager : MonoBehaviour
    {
        public GameObject lastTp;
        public bool justTp;
        public float offset;
        public int curLevel;
        public List<Transform> levels = new List<Transform>();
        [ReadOnly] public List<Transform> cameras = new List<Transform>();

        private void Start()
        {
            var i = 0;
            foreach (var level in levels)
            {
                cameras.Add(level.GetComponentInChildren<Camera>().transform);
                level.transform.position = Vector3.right * (offset * i++);
            }

            ChangeCurLevel(curLevel);
        }

        private void ChangeCurLevel(int newLevel)
        {
            if (newLevel <= 0) newLevel = 0;
            if (newLevel >= levels.Count) newLevel = levels.Count - 1;
            curLevel = newLevel;
            gameObject.layer = levels[curLevel].gameObject.layer;
        }

        private Vector3 CalculateAbsolutePlayerPos(float z)
        {
            var position = transform.position;
            var curCamOff = levels[curLevel].position;
            var pPos = new Vector3(position.x - curCamOff.x, position.y - curCamOff.y, z);
            return pPos;
        }

        private void LateUpdate()
        {
            var pPos = CalculateAbsolutePlayerPos(-10);
            foreach (var cam in cameras)
                cam.localPosition = pPos;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var tp = other.GetComponent<Tp>();
            if (justTp || tp == null)
                return;
            var pPos = CalculateAbsolutePlayerPos(0);
            ChangeCurLevel(tp.levelDest);
            transform.position = pPos + Vector3.right * (offset * curLevel);
            justTp = other.gameObject;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var tp = other.GetComponent<Tp>();
            if (!justTp || tp == null)
                return;
            justTp = false;
        }
    }
}