﻿using System.Collections;
using System.Collections.Generic;
using Firebase;
using GameManagerSystem;
using GamevrestUtils;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public List<Sprite> tools = new List<Sprite>();
        public float moveSpeed = 1;
        public float horizSpeed = .77f;
        public SpriteRenderer spriteRenderer;
        private Animator _animator;
        [ReadOnly] public Vector2 nextTranslate;
        public KeyCode changeTraceLeft = KeyCode.A;
        public KeyCode changeTraceRight = KeyCode.E;
        public KeyCode putTrace = KeyCode.Space;
        public KeyCode action = KeyCode.F;
        public SpriteRenderer inventoryRender;
        public float traceDisplayFadeTime = 1;
        public SpriteRenderer carryingRender;
        public GameObject help;
        public GameObject helpWarning;
        public GameObject helpAE;
        public GameObject toolUI;
        public GameObject gameOver;
        public int deadTime = 300; // secondes
        public SceneReference fin;

        private FirebaseMapManager _firebaseMapManager;
        private MapManager _map;
        private int _selectedTrace = 0;
        private Tool _closestTool = null;
        private Tool _currentTool = null;
        private Breakable _closestBreakable = null;
        private Crystal _closestCrystal = null;
        private static readonly int Walk = Animator.StringToHash("walk");
        private bool _dead = false;
        private static readonly int Use = Animator.StringToHash("use");
        private static readonly int Pickup = Animator.StringToHash("pickup");
        private static readonly int Die1 = Animator.StringToHash("die");

        [SerializeField] GameObject particleToPop;

        bool moving = false;

        private void Start()
        {
            _animator = spriteRenderer.GetComponent<Animator>();
            _firebaseMapManager = FindObjectOfType<FirebaseMapManager>();
            _map = FindObjectOfType<MapManager>();
            inventoryRender.gameObject.SetActive(false);
            toolUI.SetActive(false);
            StartCoroutine(DieSoon());
            InvokeRepeating("CallFootsteps", 0, 0.5f);
        }

        void CallFootsteps()
        {
            if (moving)
            {
                SoundManagement.instance.PlaySound("footStep");
            }
        }

        private IEnumerator DieSoon()
        {
            bool tuto = false;
            for (int i = 0; i < deadTime; i++)
            {
                if (i > 3 && !tuto) //time tuto
                {
                    helpAE.SetActive(false);
                    tuto = true;
                }

                var oldColor = spriteRenderer.color;
                spriteRenderer.color = new Color(oldColor.r, oldColor.g, oldColor.b, 1 - (i / (float) deadTime));
                yield return new WaitForSeconds(1);
            }

            Die();
            yield return null;
        }

        private void Update()
        {
            if (_dead)
                return;
            if (Input.GetKeyDown(changeTraceLeft))
            {
                _selectedTrace--;
                if (_selectedTrace < 0)
                    _selectedTrace = _map.traces.Count - 1;
                ChangeSelectedTraceDisplay();
            }
            else if (Input.GetKeyDown(changeTraceRight))
            {
                _selectedTrace++;
                if (_selectedTrace > _map.traces.Count - 1)
                    _selectedTrace = 0;
                ChangeSelectedTraceDisplay();
            }
            else if (Input.GetKeyDown(putTrace))
            {
                SetTrace(_selectedTrace, transform.position);
                SoundManagement.instance.PlaySound("posePanneau");
                PopParticle(particleToPop);
                SoundManagement.instance.PlaySound("utiliserObject");
                Die();
            }
            else if (Input.GetKeyDown(action))
            {
                if (_closestTool) //Pickup
                {
                    SoundManagement.instance.PlaySound("recupererObjet");
                    if (_currentTool)
                        _currentTool.Reset();
                    _currentTool = _closestTool;
                    _closestTool.SetEmpty();
                    help.SetActive(false);
                    toolUI.SetActive(true);
                    _closestTool = null;
                    ChangeCarryingDisplay();
                }
                else if (_closestBreakable && _currentTool &&
                         _closestBreakable.breakableWith == _currentTool.id) //break
                {
                    toolUI.SetActive(false);
                    carryingRender.sprite = null;
                    _animator.SetTrigger(Use);
                    _closestBreakable.ReceiveHit();
                    _dead = true;
                    SoundManagement.instance.PlaySound("utiliserObject");
                    PopParticle(particleToPop);
                    Invoke("Die", 1f);
                }
                else if (_closestCrystal) //activate crystal
                {
                    SoundManagement.instance.PlaySound("activerCrystal");
                    _closestCrystal.Activate();
                    help.SetActive(false);
                    _closestCrystal = null;
                    _dead = true;
                    SoundManagement.instance.PlaySound("utiliserObject");
                    PopParticle(particleToPop);
                    Invoke("Die", 1f);
                }
            }
        }

        private void FixedUpdate()
        {
            if (_dead)
            {
                if (Input.GetKeyDown(putTrace))
                    FindObjectOfType<SceneManager>().GoMap();
                return;
            }

            nextTranslate.x = Input.GetAxisRaw("Horizontal");
            nextTranslate.y = Input.GetAxisRaw("Vertical");
            if ((nextTranslate.x >= 1 || nextTranslate.x <= -1) &&
                (nextTranslate.y >= 1 || nextTranslate.y <= -1))
                nextTranslate = new Vector2((float) (nextTranslate.x * horizSpeed),
                    (float) (nextTranslate.y * horizSpeed));
            if (nextTranslate != Vector2.zero)
            {
                transform.Translate(new Vector3(nextTranslate.x * moveSpeed, nextTranslate.y * moveSpeed));
                moving = true;
            }
            else 
                moving = false;
            spriteRenderer.flipX = nextTranslate.x < 0;
            _animator.SetBool(Walk, nextTranslate != Vector2.zero);
        }

        private void LateUpdate()
        {
            spriteRenderer.sortingOrder = (int) (transform.position.y * -100);
        }

        private void ChangeSelectedTraceDisplay()
        {
            inventoryRender.gameObject.SetActive(true);
            inventoryRender.sprite = _map.traces[_selectedTrace];
            StopCoroutine(nameof(HideTraceDisplay));
            StartCoroutine(nameof(HideTraceDisplay));
        }

        private IEnumerator HideTraceDisplay()
        {
            yield return new WaitForSeconds(traceDisplayFadeTime);
            inventoryRender.gameObject.SetActive(false);
        }

        private void ChangeCarryingDisplay()
        {
            _animator.SetTrigger(Pickup);
            if (_currentTool.id == ToolType.Fin)
            {
                _firebaseMapManager.ResetMap();
                PersistentObject.GetSceneManager().LoadScene(fin);
                return;
            }

            var carrying = (int) _currentTool.id;
            if (carrying >= 0 && carrying <= tools.Count)
                carryingRender.sprite = tools[carrying];
        }

        //voir Map.Traces pour la liste des Traces
        private void SetTrace(int type, Vector2 pos)
        {
            _firebaseMapManager.SendTrace(type, pos);
        }

        private void Die()
        {
            _dead = true;
            _animator.SetTrigger(Die1);
            Invoke("DieDie", 1f);
        }

        void DieDie()
        {
            gameOver.SetActive(true);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Tool"))
            {
                help.SetActive(true);
                _closestTool = other.GetComponent<Tool>();
            }
            else if (other.CompareTag("Breakable") && _currentTool)
            {
                _closestBreakable = other.GetComponent<Breakable>();
                if (_closestBreakable.breakableWith == _currentTool.id)
                    helpWarning.SetActive(true);
            }
            else if (other.CompareTag("Crystal"))
            {
                _closestCrystal = other.GetComponent<Crystal>();
                help.SetActive(true);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Tool"))
            {
                help.SetActive(false);
                _closestTool = null;
            }
            else if (other.CompareTag("Breakable"))
            {
                _closestBreakable = null;
                helpWarning.SetActive(false);
            }
            else if (other.CompareTag("Crystal"))
            {
                _closestCrystal = null;
                help.SetActive(false);
            }
        }

        private void PopParticle(GameObject particleToPop)
        {
            GameObject instantiated = Instantiate(particleToPop,new Vector3(0, 0, 0), new Quaternion(0,0,0,0));
            instantiated.transform.Rotate(new Vector3(0,0,0),Space.Self);
            instantiated.transform.parent = gameObject.transform;
            instantiated.transform.localScale = new Vector3(1,1,1);
            Destroy(instantiated, instantiated.GetComponent<ParticleSystem>().main.duration + instantiated.GetComponent<ParticleSystem>().main.startLifetime.constantMax);
        }
    }
}