﻿using GameManagerSystem;
using GamevrestUtils;
using UnityEngine;

namespace UI
{
    public class CharacterCreationManager : MonoBehaviour
    {
        public SceneReference nextScene;
        public string playerName;
        
        public void Confirm()
        {
            PersistentObject.GetGameData().CreatePlayer(playerName);
            PersistentObject.GetSceneManager().LoadScene(nextScene);
        }

        public void MajName(string newName)
        {
            playerName = newName;
        }
    }
}