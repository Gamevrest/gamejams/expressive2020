﻿using System;
using Firebase.Database;
using GameManagerSystem;
using GamevrestUtils;
using UI;
using UnityEngine;
using UnityEngine.Events;
using SceneManager = UnityEngine.SceneManagement.SceneManager;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Firebase
{
    [Serializable]
    public class ReceiveTraceCallback : UnityEvent<int, Vector3, string, DateTime>
    {
    }

    [Serializable]
    public class ReceiveObjectMajCallback : UnityEvent<string, object, string, DateTime>
    {
    }

    public class FirebaseMapManager : MonoBehaviour
    {
        [Header("Firebase")] public string mapsPath = "Maps";
        [ReadOnly] public string mapName;
        public string obstaclesPath = "obstacles";
        public string tracesPath = "traces";

        public DatabaseReference mapRoot;
        public DatabaseReference obstaclesRoot;
        public DatabaseReference tracesRoot;

        public ReceiveTraceCallback traceCallback;
        public ReceiveObjectMajCallback objectMajCallback;
        [Header("notifs")] public PopupDisplayer displayer;
        public string username;
        public bool ignoreSelf = true;

        private void Start()
        {
            username = PersistentObject.GetGameData().savedData.userName;
            mapName = SceneManager.GetActiveScene().name;
            mapRoot = PersistentObject.GetGameData().databaseRoot.Child(mapsPath).Child(mapName);
            obstaclesRoot = mapRoot.Child(obstaclesPath);
            tracesRoot = mapRoot.Child(tracesPath);

            obstaclesRoot.ChildChanged += ObstaclesRootOnChildChanged;
            obstaclesRoot.ChildAdded += ObstaclesRootOnChildChanged;
            // tracesRoot.ChildChanged += TracesRootOnChildChanged;
            tracesRoot.ChildAdded += TracesRootOnChildChanged;
        }

        public void ResetMap()
        {
            mapRoot.RemoveValueAsync();
        }

        private void ObstaclesRootOnChildChanged(object sender, ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            var id = args.Snapshot.Key;
            var value = args.Snapshot.Child("value").Value;
            var user = args.Snapshot.Child("user").Value.ToString();
            var date = DateTimeOffset
                .FromUnixTimeMilliseconds(long.Parse(args.Snapshot.Child("timestamp").Value.ToString())).UtcDateTime
                .ToLocalTime();
//            print($"obj maj : [{id}] -> [{value}]");
            objectMajCallback.Invoke(id, value, ignoreSelf && user == username ? "" : user, date);
        }

        private void TracesRootOnChildChanged(object sender, ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            var type = Convert.ToInt32(args.Snapshot.Child("type").Value);
            var x = (float) Convert.ToDouble(args.Snapshot.Child("x").Value);
            var y = (float) Convert.ToDouble(args.Snapshot.Child("y").Value);
            var z = (float) Convert.ToDouble(args.Snapshot.Child("z").Value);
            var user = args.Snapshot.Child("user").Value.ToString();
            var date = DateTimeOffset
                .FromUnixTimeMilliseconds(long.Parse(args.Snapshot.Child("timestamp").Value.ToString())).UtcDateTime
                .ToLocalTime();
            //print($"new trace : [{type}] -> [{new Vector3(x, y, z)}]");
            traceCallback.Invoke(type, new Vector3(x, y, z), ignoreSelf && user == username ? "" : user, date);
        }

        public void SendObstacleMaj(string id, object value)
        {
            var json = $"{{\"value\":{value},\"user\":\"{username}\",\"timestamp\":{{\".sv\":\"timestamp\"}}}}";
            obstaclesRoot.Child(id).SetRawJsonValueAsync(json).ContinueWith(GamevrestTools.FirebaseTaskReport);
        }

        public void SendTrace(int type, Vector3 position)
        {
            var json =
                $"{{\"type\":\"{type}\",\"x\":{position.x},\"y\":{position.y},\"z\":{position.z},\"user\":\"{username}\",\"timestamp\":{{\".sv\":\"timestamp\"}}}}";
            print(json);
            tracesRoot.Push().SetRawJsonValueAsync(json).ContinueWith(GamevrestTools.FirebaseTaskReport);
        }
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(FirebaseMapManager))]
    public class FirebaseMapManagerInspector : Editor
    {
        private string _id = "rock";
        private float _value = 450;

        private int _type = 0;
        private Vector2 _position = Vector2.one;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var targ = target as FirebaseMapManager;
            if (targ == null)
                return;

            CustomEditorUtils.DrawHeader("Controls (play only)");
            if (!Application.isPlaying) return;
            //using (new GUILayout.HorizontalScope())
            {
                _id = GUILayout.TextField(_id);
                _value = EditorGUILayout.FloatField(_value);
                if (GUILayout.Button("SendObstacleMaj"))
                    targ.SendObstacleMaj(_id, _value);
            }

            // using (new GUILayout.HorizontalScope())
            {
                _type = EditorGUILayout.IntField(_type);
                _position = EditorGUILayout.Vector2Field("", _position);
                if (GUILayout.Button("SendTrace"))
                    targ.SendTrace(_type, _position);
            }

            if (GUILayout.Button("ResetMap"))
                targ.ResetMap();
        }
    }
#endif
}