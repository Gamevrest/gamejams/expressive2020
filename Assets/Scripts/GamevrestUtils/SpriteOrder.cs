﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace GamevrestUtils
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteOrder : MonoBehaviour
    {
        public bool automatic = true;
        private SpriteRenderer _rend;

        // private void Awake()
        // {
        //     CheckRenderer();
        // }
        // private void OnBecameVisible()
        // {
        //     CheckRenderer();
        // } // private void OnBecameVisible()
        // {
        //     CheckRenderer();
        // }

        public void CheckRenderer()
        {
            if (_rend == null)
                _rend = GetComponent<SpriteRenderer>();
            if (!automatic) return;
            _rend.sortingOrder = (int) (transform.position.y * -100);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(SpriteOrder))]
    public class MeshSortingOrderExample : Editor
    {
        private SpriteRenderer _rend;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            var targ = target as SpriteOrder;
            if (targ == null) return;
            targ.CheckRenderer();
            serializedObject.ApplyModifiedProperties();
            if (GUILayout.Button("check render"))
                targ.CheckRenderer();
        }
    }
#endif
}