﻿using UnityEngine;

public class SoundManagement : MonoBehaviour
{
    [SerializeField] AudioClip footStep;
    [Range(0f, 1f)] [SerializeField] float footStepVolume;
    [SerializeField] AudioClip posePanneau;
    [Range(0f, 1f)] [SerializeField] float posePanneauVolume;
    [SerializeField] AudioClip cassageMur;
    [Range(0f, 1f)] [SerializeField] float cassageMurVolume;
    [SerializeField] AudioClip brulerRonces;
    [Range(0f, 1f)] [SerializeField] float brulerRoncesVolume;
    [SerializeField] AudioClip creuser;
    [Range(0f, 1f)] [SerializeField] float creuserVolume;
    [SerializeField] AudioClip couperArbre;
    [Range(0f, 1f)] [SerializeField] float couperArbreVolume;
    [SerializeField] AudioClip piocherRocher;
    [Range(0f, 1f)] [SerializeField] float piocherRocherVolume;
    [SerializeField] AudioClip activerCrystal;
    [Range(0f, 1f)] [SerializeField] float activerCrystalVolume;
    [SerializeField] AudioClip ouvrirPorte;
    [Range(0f, 1f)] [SerializeField] float ouvrirPorteVolume;
    [SerializeField] AudioClip recupererObjet;
    [Range(0f, 1f)] [SerializeField] float recupererObjetVolume;
    [SerializeField] AudioClip utiliserObjet;
    [Range(0f, 1f)] [SerializeField] float utiliserObjetVolume;

    public static SoundManagement instance = null;

    AudioSource source;

    float pitchMin = 0.9f;
    float pitchMax = 1.1f;

    void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else 
		{
			Destroy (gameObject);
		}
	}
    
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlaySound(string audioName)
    {
        switch(audioName)
		{
		case "footStep":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = footStepVolume;
			source.PlayOneShot (footStep);
			break;
		case "posePanneau":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = posePanneauVolume;
			source.PlayOneShot (posePanneau);
			break;
		case "cassageMur":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = cassageMurVolume;
			source.PlayOneShot (cassageMur);
			break;
		case "brulerRonces":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = brulerRoncesVolume;
			source.PlayOneShot (brulerRonces);
			break;
		case "creuser":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = creuserVolume;
			source.PlayOneShot (creuser);
			break;
		case "couperArbre":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = couperArbreVolume;
			source.PlayOneShot (couperArbre);
			break;
		case "piocherRocher":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = piocherRocherVolume;
			source.PlayOneShot (piocherRocher);
			break;
		case "activerCrystal":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = activerCrystalVolume;
			source.PlayOneShot (activerCrystal);
			break;
		case "ouvrirPorte":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = ouvrirPorteVolume;
			source.PlayOneShot (ouvrirPorte);
			break;
		case "recupererObjet":
			source.pitch = Random.Range (pitchMin, pitchMax);
			source.volume = recupererObjetVolume;
			source.PlayOneShot (recupererObjet);
			break;
		case "utiliserObjet":
			source.pitch = Random.Range (pitchMin, pitchMax);
            source.volume = utiliserObjetVolume;
            source.PlayOneShot(utiliserObjet);
            break;
            
		default:
			break;
		}
	
    }
}
