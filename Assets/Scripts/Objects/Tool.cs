﻿using UnityEngine;

public enum ToolType { Pioche, Hache, Marteau, Pelle, Rune, Fin};
public class Tool : MonoBehaviour
{
    public ToolType id;
    public Sprite fullState;
    public Sprite emptyState;
    private SpriteRenderer render;
    private void Start()
    {
        render = GetComponent<SpriteRenderer>();
        render.sprite = fullState;
    }

    public void SetEmpty()
    {
        render.sprite = emptyState;
        gameObject.tag = "Untagged";
    }

    public void Reset()
    {
        render.sprite = fullState;
        gameObject.tag = "Tool";
    }
}
