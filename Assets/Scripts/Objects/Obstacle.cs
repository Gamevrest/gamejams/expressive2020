﻿using System;
using Firebase;
using UI;
using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
    protected FirebaseMapManager firebaseMapManager;
    [Header("notifs")] public PopupDisplayer displayer;

    private void Awake()
    {
        displayer = FindObjectOfType<PopupDisplayer>();
        firebaseMapManager = FindObjectOfType<FirebaseMapManager>();
        FindObjectOfType<MapManager>().AddObstacle(this);
    }

    public abstract void SendSelf();
    public abstract void ReceiveSelf(object newContent, string user, DateTime date);
}
