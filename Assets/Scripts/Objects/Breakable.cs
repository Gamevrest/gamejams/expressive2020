﻿using System;
using GamevrestUtils;
using UnityEngine;

[RequireComponent(typeof(DecorObject))]
public class Breakable : Obstacle
{
    [ReadOnly] private int life = 5;
    public int lifeMax = 5;
    public ToolType breakableWith = ToolType.Pioche;
    private SoundManagement _soundManagement;
    private DecorObject decor;
    private BoxCollider2D box;
    private void Start()
    {
        life = lifeMax;
        decor = GetComponent<DecorObject>();
        box = GetComponent<BoxCollider2D>();
    }

    public override void SendSelf()
    {
        firebaseMapManager.SendObstacleMaj(name, life);
    }

    public override void ReceiveSelf(object newLife, string user, DateTime date)
    {
        life = Convert.ToInt32(newLife);
        if (!string.IsNullOrWhiteSpace(user) && date >= DateTime.Now)
        {
            //todo changer la notif ici
            displayer.InstantiateNotification($"{user}:action on obstacle {name}");
        }
        ObjectLogic();
    }

    public void ReceiveHit()
    {
        life--;
        ObjectLogic();
        SendSelf();
    }

    private void ObjectLogic()
    {
        UpdateSprite();

        switch(decor.type)
        {
            case DecorType.Stone:
                SoundManagement.instance.PlaySound("piocherRocher");
                break;
            case DecorType.Bush:
                SoundManagement.instance.PlaySound("brulerRonces");
                break;
            case DecorType.Trunc:
                SoundManagement.instance.PlaySound("couperArbre");
                break;
            case DecorType.Wall:
                SoundManagement.instance.PlaySound("cassageMur");
                break;
            default:
                break;
        }

        if (life <= 0)
        {
            decor.SetDead();
            box.enabled = false;
            //gameObject.SetActive(false);
        }
        else if (!gameObject.activeSelf)
        {
            box.enabled = true;
            //gameObject.SetActive(true);
        }
    }

    private void UpdateSprite()
    {
        GetComponent<DecorObject>().SetSpriteState((float) life / (float) lifeMax);
    }
}