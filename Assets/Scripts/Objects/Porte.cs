﻿using UnityEngine;

public class Porte : MonoBehaviour
{
    public Crystal portail1;
    public Crystal portail2;

    private void Update()
    {
        if (portail1.activated == 1 && portail2.activated == 1)
            Destroy(gameObject);
    }
}