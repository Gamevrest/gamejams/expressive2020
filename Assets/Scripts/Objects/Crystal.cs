﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Crystal : Obstacle
{
    public List<Sprite> sprites = new List<Sprite>();
    public int activated = 0;

    private SpriteRenderer render;

    private void Start()
    {
        render = GetComponent<SpriteRenderer>();
    }

    public override void SendSelf()
    {
        firebaseMapManager.SendObstacleMaj(name, activated);
    }

    public override void ReceiveSelf(object newState, string user, DateTime date)
    {
        activated = Convert.ToInt32(newState);
        if (!string.IsNullOrWhiteSpace(user) && date >= DateTime.Now)
        {
            //todo changer la notif ici
            displayer.InstantiateNotification($"{user}:action on obstacle {name}");
        }

        ObjectLogic();
    }

    public void Activate()
    {
        activated = 1;
        ObjectLogic();
        SendSelf();
    }

    private void ObjectLogic()
    {
        if (activated == 1 && sprites.Count > 1)
            render.sprite = sprites[1];
    }
}