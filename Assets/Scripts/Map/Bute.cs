﻿using GamevrestUtils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Map
{
    public class Bute : MonoBehaviour
    {
        [Header("UP")] public Sprite midU;
        public Sprite rightU;
        [Header("MIDDLE")] public Color midM;
        public Sprite rightM;
        [Header("DOWN")] public Sprite midD;
        public Sprite rightD;
        [Header("Others infos")] public GameObject prefab;
        public Vector2 size;
        public Vector2 offset;

        public void Refresh()
        {
            if (size.x <= 0) size.x = 1;
            if (size.y <= 0) size.y = 1;
            if (offset.x <= 0) offset.x = 1;
            if (offset.y <= 0) offset.y = 1;
            var max = offset * (size + Vector2.one);
            GamevrestTools.CleanChildren(transform);
            //left down
            InstantiateSprite(rightD, Vector2.zero, true);
            //right down
            InstantiateSprite(rightD, new Vector2(max.x, 0));
            //left up
            InstantiateSprite(rightU, new Vector2(0, max.y), true);
            //right up
            InstantiateSprite(rightU, max);

            for (float x = 1; x <= size.x; x++)
            {
                InstantiateSprite(midU, new Vector2(x * offset.x, max.y + offset.y / 1.5f));
                InstantiateSprite(midD, new Vector2(x * offset.x, 0));
            }

            for (float y = 1; y <= size.y; y++)
            {
                InstantiateSprite(rightM, new Vector2(max.x, y * offset.y));
                InstantiateSprite(rightM, new Vector2(0, y * offset.y), true);
            }
        }

        public void InstantiateSprite(Sprite sprite, Vector2 position, bool flipX = false)
        {
            var obj = Instantiate(prefab, transform);
            obj.transform.localPosition = position;
            obj.name = $"{sprite.name} {flipX}";
            var rend = obj.GetComponent<SpriteRenderer>();
            if (rend)
            {
                rend.sprite = sprite;
                rend.flipX = flipX;
            }

            var order = obj.GetComponent<SpriteOrder>();
            if (order)
                order.CheckRenderer();
        }
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(Bute))]
    public class ButeInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var targ = target as Bute;
            if (targ == null)
                return;
            if (GUILayout.Button("Refresh"))
                targ.Refresh();
        }
    }
#endif
}