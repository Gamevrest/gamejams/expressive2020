﻿using System.Collections.Generic;
using GamevrestUtils;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class StateList
{
    public List<Sprite> states = new List<Sprite>();
}

[System.Serializable]
public class SpriteList
{
    public DecorType type;
    public List<StateList> sprites = new List<StateList>();
}

public class SpriteFactory : MonoBehaviour
{
    public List<SpriteList> spritesReferences = new List<SpriteList>();
    [Header("Edit")]public int randomSeed = 0;
    public float perlinMult = 10;
    [ReadOnly] private List<DecorObject> decorObjects = new List<DecorObject>();

    public void AddObstacle(DecorObject decorObject)
    {
        if (!decorObjects.Contains(decorObject))
            decorObjects.Add(decorObject);
    }

    public void ResetReferences()
    {
        var decorRefs = FindObjectsOfType<DecorObject>();
        decorObjects = new List<DecorObject>();
        foreach (var decorRef in decorRefs)
            AddObstacle(decorRef);
    }

    public List<Sprite> GetStatesByTypeAndState(DecorType type, int state)
    {
        foreach (var sprites in spritesReferences)
            if (sprites.type == type)
                return sprites.sprites[state].states;
        return null;
    }
    
    public void SetAllSpritesRandomly()
    {
        if (randomSeed != 0)
            Random.InitState(randomSeed);
        var offset = Random.Range(-1000, 1000);
        foreach (var decor in decorObjects)
        {
            foreach (var sprites in spritesReferences)
            {
                if (sprites.type == decor.type)
                {
                    if (!decor.ManuallySet)
                    {
                        var position = decor.transform.position / perlinMult;
                        var select = Mathf.PerlinNoise(position.x + offset, position.y + offset) * sprites.sprites.Count;
                        select = select < 0 || select >= sprites.sprites.Count ? 0 : select;
                        int randomSprites = (int)select;
                        var states = sprites.sprites[randomSprites].states;
                        decor.SetSprite(states[states.Count - 1], randomSprites);
                        break;
                    }
                }
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SpriteFactory))]
public class SpriteFactoryInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var targ = target as SpriteFactory;
        if (targ == null)
            return;
        if (GUILayout.Button("ResetReferences"))
            targ.ResetReferences();
        if (GUILayout.Button("SetAllSpriteRandomly"))
            targ.SetAllSpritesRandomly();
    }
}
#endif
