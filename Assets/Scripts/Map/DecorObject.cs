﻿using System;
using GamevrestUtils;
using UnityEditor;
using UnityEngine;

public enum DecorType
{
    Arbre,
    Stone,
    Bush,
    Trunc,
    Wall
};

[Serializable]
public class DecorObject : MonoBehaviour
{
    public int state; //"couleur" de l'élément, index du set de sprite

    public DecorType type;
    public bool ManuallySet = false;
    [HideInInspector] public SpriteRenderer spriteRenderer;
    public SpriteFactory spriteFactory;
    private SpriteOrder _spriteOrder;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (name.Contains("Arbre"))
            return;
        spriteFactory = FindObjectOfType<SpriteFactory>();
    }

    public void SetSpriteState(float lifePercentage)
    {
        var states = spriteFactory.GetStatesByTypeAndState(type, state);
        if (states == null)
            return;
        var count = states.Count - 1;
        var tmp = (int) (count * lifePercentage);
        tmp = tmp < 0 || tmp > count ? count : tmp;
        spriteRenderer.sprite = states[tmp + 1];
    }

    public void SetDead()
    {
        var states = spriteFactory.GetStatesByTypeAndState(type, state);
        if (states == null)
            return;
        spriteRenderer.sprite = states[0];
    }


    public void SetSprite(Sprite sprite, int state)
    {
        this.state = state;
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
        if (_spriteOrder == null) _spriteOrder = GetComponent<SpriteOrder>();
        spriteRenderer.sprite = sprite; //states[states.Count - 1];
        if (_spriteOrder == null)
        {
            Debug.LogError(name);
        }

        _spriteOrder.CheckRenderer();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(DecorObject))]
public class DecorObjectInspector : Editor
{
    public void OnEnable()
    {
        var targ = target as DecorObject;
        if (targ == null)
            return;
        targ.spriteRenderer = targ.GetComponent<SpriteRenderer>();
        // FindObjectOfType<SpriteFactory>().AddObstacle(targ);
    }
}
#endif