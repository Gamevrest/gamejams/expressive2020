﻿using System;
using System.Collections.Generic;
using GamevrestUtils;
using UI;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public GameObject tracePrefab;
    public List<Sprite> traces = new List<Sprite>();

    public List<Transform> traceParents = new List<Transform>();
    private List<Obstacle> obstacles = new List<Obstacle>();
    [Header("notifs")] public PopupDisplayer displayer;

    private void Start()
    {
        displayer = FindObjectOfType<PopupDisplayer>();
    }


    public void ReceiveTrace(int type, Vector3 content, string user, DateTime date)
    {
        if (!string.IsNullOrWhiteSpace(user) && date >= DateTime.Now)
        {
            //todo changer la notif ici
            displayer.InstantiateNotification($"{user}:put {traces[type].name} trace on {content}");
        }

        InstantiateTraceSimple(type, content);
    }

    public void ReceiveObjectUpdate(string id, object value, string user, DateTime date)
    {
        foreach (var obstacle in obstacles)
        {
            if (obstacle == null || obstacle.name != id)
                continue;
            obstacle.ReceiveSelf(value, user, date);
            return;
        }

        Debug.Log($"Obstacle {id} not found");
    }

    void InstantiateTraceSimple(int type, Vector3 pos)
    {
        if (type >= traces.Count) return;
        var trace = Instantiate(tracePrefab, traceParents[(int) pos.z]);
        var render = trace.GetComponent<SpriteRenderer>();
        render.sprite = traces[type];
        var order = trace.GetComponent<SpriteOrder>();
        order.CheckRenderer();
        trace.transform.localPosition = new Vector3(pos.x, pos.y, 0);
    }

    public void AddObstacle(Obstacle obstacle)
    {
        obstacles.Add(obstacle);
    }
}