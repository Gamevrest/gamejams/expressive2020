﻿using GamevrestUtils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif
namespace Map
{
    public class Wall : MonoBehaviour
    {
        public GameObject prefab;
        public Vector2 size;
        public Vector2 number;

        private BoxCollider2D _collider2D;

        public void Refresh()
        {
            if (size.x <= 0) size.x = 1;
            if (size.y <= 0) size.y = 1;
            if (number.x <= 0) number.x = 1;
            if (number.y <= 0) number.y = 1;
            GamevrestTools.CleanChildren(transform);
            var step = size / number;
            for (var x = -size.x / 2; x < size.x / 2; x += step.x)
            {
                for (var y = -size.y / 2; y < size.y / 2; y += step.y)
                {
                    var obj = Instantiate(prefab, transform);
                    obj.transform.localPosition = new Vector3(x + step.x / 2, y, 0);
                    var script = obj.GetComponent<SpriteOrder>();
                    if (script)
                        script.CheckRenderer();
                }
            }

            _collider2D = GetComponent<BoxCollider2D>();
            _collider2D.offset = Vector2.zero;
            _collider2D.size = size;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Wall))]
    public class WallInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var targ = target as Wall;
            if (targ == null)
                return;
            if (GUILayout.Button("Refresh"))
                targ.Refresh();
        }
    }
#endif
}