﻿using Firebase;
using Firebase.Database;
using GamevrestUtils;
using SaveSystem;
using UnityEngine;

namespace GameManagerSystem
{
    public class GameData : MonoBehaviour
    {
        [Header("Game Data")] [ReadOnly] public SaveData savedData;
        [Header("Firebase")] public string databaseUrl;
        public DatabaseReference databaseRoot;

        private void Awake()
        {
           // FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(databaseUrl);
            databaseRoot = FirebaseDatabase.DefaultInstance.RootReference;
#if UNITY_EDITOR
            Load();
#endif
        }

        public void CreatePlayer(string newUser)
        {
            savedData = new SaveData {userName = newUser};
            Save();
        }

        public void Save()
        {
            SaveManager.Save(savedData);
        }

        public void Load()
        {
            savedData = SaveManager.Load();
            if (string.IsNullOrEmpty(savedData.userName))
                savedData.userName = "Default";
        }
    }
}